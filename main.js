var per=[];//periodo
var saldoI=[];//saldo Insoluto
var Amort=[];//Amortizacion
const RellenarTabla=document.querySelector('#tabla tbody');
Amortizacion(mon=20000,pl=12);
function Amortizacion(){
    Amort=mon/pl;
    for(let i=1;i<pl+1;i++){
        if (i<=1){
            saldoI[i]=mon;
        }else {
         saldoI[i]=saldoI[i-1]-Amort;
        }
    }
}
var diasE=[];//dias entre fechas
var fechaI=[];//Fecha Inicial
var FechaInicio=moment('2021-08-03');
var fechaF=[];//Fecha Final
var FechaFinal=moment('2021-09-03');
//Fechas
Fechas();
function Fechas(){
    for (i=1;i<pl+1;i++){
    per[i]=[i];
    fechaI[i]=FechaInicio.format('DD-MM-YYYY');
    fechaF[i]=FechaFinal.format('DD-MM-YYYY');
    diasE[i]=(FechaFinal.diff(FechaInicio,'days'));
    FechaInicio.add(1,'month');
    FechaFinal.add(1, 'month');
    }
} 
var interesAnual=(0.12);
var intereses=[];
var iva=[];
var flujo=[];
calcular();
function calcular(){
    for (i=1;i<pl+1;i++){
        intereses[i]=saldoI[i]*diasE[i]*(interesAnual/360);
        iva[i]=intereses[i]*.16;
        flujo[i]=Amort+intereses[i]+iva[i];
    }
}
var disp=(0);
for (i=1;i<pl+1;i++){
const row1=document.createElement('tr')
    row1.innerHTML=`
    <td>${per[i]}</td><td>${fechaI[i]}</td><td>${fechaF[i]}</td><td>${diasE[i]}</td><td>${disp}</td><td>${saldoI[i]}</td><td>${Amort},</td><td>${intereses[i]}</td><td>${iva[i]}</td><td>${flujo[i]}</td>
    `;
    RellenarTabla.appendChild(row1);
}